﻿iRowCount = Datatable.getSheet("TC01 [TC01_BorrowingPowerCalculator]").getRowCount
''----------------------------- Data ------------------------------'
LoanPersonIncome = Trim((DataTable("LoanPersonIncome","TC01 [TC01_BorrowingPowerCalculator]")))
ExpensePeriodAmount = Trim((DataTable("ExpensePeriodAmount","TC01 [TC01_BorrowingPowerCalculator]")))
InterestRate = Trim((DataTable("InterestRate","TC01 [TC01_BorrowingPowerCalculator]")))
LoanTotalYear = Trim((DataTable("LoanTotalYear","TC01 [TC01_BorrowingPowerCalculator]")))
ExpectedLoanPersonIncome = Trim((DataTable("ExpectedLoanPersonIncome","TC01 [TC01_BorrowingPowerCalculator]")))
ExpectedExpensePeriodAmount = Trim((DataTable("ExpectedExpensePeriodAmount","TC01 [TC01_BorrowingPowerCalculator]")))
ExpectedInterestRate = Trim((DataTable("ExpectedInterestRate","TC01 [TC01_BorrowingPowerCalculator]")))
ExpectedLoanTotalYear = Trim((DataTable("ExpectedLoanTotalYear","TC01 [TC01_BorrowingPowerCalculator]")))
ExpectedTotalApprovableAmount = Trim((DataTable("ExpectedTotalApprovableAmount","TC01 [TC01_BorrowingPowerCalculator]")))
ExpectedPeriodAmount = Trim((DataTable("ExpectedPeriodAmount","TC01 [TC01_BorrowingPowerCalculator]")))
'
''----------------------------- Data ------------------------------'
Call FW_OpenWebBrowser("เปิดเว็บ","https://www.krungsri.com/bank/th/Other/Calculator/Loancalculator/BorrowingPowerCalculator.html","IE")
Call FW_WebEditAndVerifyResult ("กรอกรายได้ต่อเดือน","คำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","รายได้ต่อเดือน",LoanPersonIncome,ExpectedLoanPersonIncome) @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_WebEditAndVerifyResult ("กรอกค่าใช้จ่ายต่อเดือน","คำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","ค่าใช้จ่ายต่อเดือน",ExpensePeriodAmount,ExpectedExpensePeriodAmount)
Call FW_WebEditAndVerifyResult ("กรอกดอกเบี้ยที่ใช้ในการคำนวณ","คำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","ดอกเบี้ยที่ใช้ในการคำนวณ",InterestRate,ExpectedInterestRate) @@ script infofile_;_ZIP::ssf4.xml_;_
Call FW_WebEditAndVerifyResult ("กรอกระยะเวลาที่ขอกู้","คำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","ระยะเวลาที่ขอกู้ (ปี)",LoanTotalYear,ExpectedLoanTotalYear) @@ script infofile_;_ZIP::ssf5.xml_;_
Call FW_WebButton ("กดปุ่มเริ่มคำนวณ","คำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","เริ่มคำนวณ")
Call FW_ValidateElementMessage ("ตรวจสอบคำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","ท่านสามารถกู้ได้",ExpectedTotalApprovableAmount) @@ script infofile_;_ZIP::ssf7.xml_;_
Call FW_ValidateElementMessage ("ตรวจสอบคำนวณค่างวดต่อเดือน","คำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","ค่างวดต่อเดือน",ExpectedPeriodAmount) @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_CloseWebBrowserIE("ปิดเว็บ")



