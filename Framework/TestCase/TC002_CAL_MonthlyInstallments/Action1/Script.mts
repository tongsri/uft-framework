﻿iRowCount = Datatable.getSheet("TC002 [TC002_CAL_MonthlyInstallments]").getRowCount
'คำนวณจากราคารถยนต์
tabNameData = Trim((DataTable("TabName","TC002 [TC002_CAL_MonthlyInstallments]")))
'ค่างวดรถต่อเดือน
carPricePerMonthData = Trim((DataTable("CarPricePerMonth","TC002 [TC002_CAL_MonthlyInstallments]")))
'เงินดาวน์
downPaymentData = Trim((DataTable("DownPayment","TC002 [TC002_CAL_MonthlyInstallments]")))
'ดอกเบี้ย
interestData = Trim((DataTable("Interest","TC002 [TC002_CAL_MonthlyInstallments]")))
'เลือกผ่อน 12,24,36,48,60,72
'monthData = Trim((DataTable("Month","TC002 [TC002_CAL_MonthlyInstallments]")))

'Excepted ถ้ามีงบต่อเดือนเท่านี้ จะซื้อรถได้ประมาณราคาเท่าไหร่
expectedMessageData = Trim((DataTable("ExpectedMessage","TC002 [TC002_CAL_MonthlyInstallments]")))
'Excepted ค่างวดรถต่อเดือน
expectedCarPricePerMonthData = Trim((DataTable("ExpectedCarPricePerMonth","TC002 [TC002_CAL_MonthlyInstallments]")))
'Excepted เงินดาวน์
expectedDownPaymentData = Trim((DataTable("ExpectedDownPayment","TC002 [TC002_CAL_MonthlyInstallments]")))
'Excepted ดอกเบี้ย
expectedInterestData = Trim((DataTable("ExpectedInterest","TC002 [TC002_CAL_MonthlyInstallments]")))
'Excepted เลือกผ่อน 12,24,36,48,60,72
expectedMonthData = Trim((DataTable("ExpectedMonth","TC002 [TC002_CAL_MonthlyInstallments]")))

expectedHighPriceData = Trim((DataTable("ExpectedHighPrice","TC002 [TC002_CAL_MonthlyInstallments]")))

'selectMonth = ""
'Select Case monthData
'	'12	
'	Case "12"
'	selectMonth = "0"
'	'24	
'	Case "24"
'	selectMonth = "1"
'	'36	
'	Case "36"
'	selectMonth = "2"
'	'48	
'	Case "48"
'	selectMonth = "3"
'	'60	
'	Case "60"
'	selectMonth = "4"
'	'72	
'	Case "72"
'	selectMonth = "5"
'End Select

'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebElement("คำนวณจากค่างวดต่อเดือน").Click
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebTabStrip("คำนวณจากค่างวดต่อเดือน").Select "คำนวณจากค่างวดต่อเดือน" @@ script infofile_;_ZIP::ssf1.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("ค่างวดต่อเดือน(รวมVAT)").Set "20,000" @@ script infofile_;_ZIP::ssf2.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("เงินดาวน์").Set "200,000" @@ script infofile_;_ZIP::ssf3.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebEdit("ดอกเบี้ย").Set "3" @@ script infofile_;_ZIP::ssf5.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebButton("คำนวนสินเชื่อ").Click @@ script infofile_;_ZIP::ssf6.xml_;_
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebRadioGroup("listloan").Select "#4"
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebElement("ถ้ามีงบต่อเดือนเท่านี้").Click

'Set strMonth = 
'Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebTable("ตารางแสดงระยะเวลาการผ่อนชำระ").GetCellData(6,2)
'Set Object = Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebTable("ตารางแสดงระยะเวลาการผ่อนชำระ")
'rowCount = Object.RowCount
'columnCount = Object.ColumnCount
'Print "rowCount --> " & rowCount
'Print "columnCount --> " & columnCount
'For i = 1 To rowCount Step 1
'	For j = 1 To columnCount Step 1
'		data = Browser("กรุงศรี มาร์เก็ต รถมือสอง").Page("กรุงศรี มาร์เก็ต รถมือสอง").WebTable("ตารางแสดงระยะเวลาการผ่อนชำระ").GetCellData(i,j)
'		print "data --> " & data
'	Next
'Next

Call FW_OpenWebBrowser("เปิดเวปกรุงศรี มาร์เก็ต รถมือสอง","https://www.krungsrimarket.com/CalculateLoan","CHROME")
'Call FW_ValidateElementMessage("ตรวจสอบแถบคำนวณจากค่างวดต่อเดือน","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากค่างวดต่อเดือน", "คำนวณจากค่างวดต่อเดือน")
Call FW_WebTabStripAndVerifyElementResult("เลือกคำนวณจากค่างวดต่อเดือน","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวณจากค่างวดต่อเดือน", "ถ้ามีงบต่อเดือนเท่านี้", tabNameData, expectedMessageData)
Call FW_WebEditAndVerifyResult("กรอกค่างวดต่อเดือน(รวมVAT)","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ค่างวดต่อเดือน(รวมVAT)", carPricePerMonthData, expectedCarPricePerMonthData)
Call FW_WebEditAndVerifyResult("กรอกเงินดาวน์","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","เงินดาวน์", downPaymentData, expectedDownPaymentData)
Call FW_WebEditAndVerifyResult("กรอกดอกเบี้ย","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","ดอกเบี้ย", interestData, expectedInterestData)
Call FW_WebButtonAndVerifyTableResult("กดปุ่มคำนวนสินเชื่อ","กรุงศรี มาร์เก็ต รถมือสอง","กรุงศรี มาร์เก็ต รถมือสอง","คำนวนสินเชื่อ", "ตารางแสดงระยะเวลาการผ่อนชำระ", expectedMonthData, expectedHighPriceData)
Call FW_CloseWebBrowserChrome("ปิดเวปกรุงศรี มาร์เก็ต รถมือสอง")
 @@ script infofile_;_ZIP::ssf12.xml_;_
