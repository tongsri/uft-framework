﻿iRowCount = Datatable.getSheet("TC02 [TC02_Paymonthlyinstallments]").getRowCount
''----------------------------- Data ------------------------------'
Text = Trim((DataTable("Text","TC02 [TC02_Paymonthlyinstallments]")))
LoanAmount = Trim((DataTable("LoanAmount","TC02 [TC02_Paymonthlyinstallments]")))
InterestRate = Trim((DataTable("InterestRate","TC02 [TC02_Paymonthlyinstallments]")))
LoanTotalYear = Trim((DataTable("LoanTotalYear","TC02 [TC02_Paymonthlyinstallments]")))
ExpectedLoanAmount = Trim((DataTable("ExpectedLoanAmount","TC02 [TC02_Paymonthlyinstallments]")))
ExpectedInterestRate = Trim((DataTable("ExpectedInterestRate","TC02 [TC02_Paymonthlyinstallments]")))
ExpectedLoanTotalYear = Trim((DataTable("ExpectedLoanTotalYear","TC02 [TC02_Paymonthlyinstallments]")))
ExpectedPeriodAmount = Trim((DataTable("ExpectedPeriodAmount","TC02 [TC02_Paymonthlyinstallments]")))
'
''----------------------------- Data ------------------------------'


Call FW_OpenWebBrowser("เปิดเว็บ","https://www.krungsri.com/bank/th/Other/Calculator/Loancalculator/BorrowingPowerCalculator.html","IE")

Call FW_Link ("ความสามารถในการผ่อนชำระต่อเดือน","คำนวณความสามารถในการกู้","คำนวณความสามารถในการผ่อนชำระ","ความสามารถในการผ่อนชำระต่อเดือน")
'Browser("คำนวณความสามารถในการกู้").Page("คำนวณความสามารถในการผ่อนชำระ").Link("ความสามารถในการผ่อนชำระต่อเดือน").Click

Call FW_WebEditAndVerifyResult ("กรอกวงเงินที่ขอกู้","คำนวณความสามารถในการกู้","คำนวณความสามารถในการผ่อนชำระ","LoanAmount",LoanAmount,ExpectedLoanAmount)
'Browser("คำนวณความสามารถในการกู้").Page("คำนวณความสามารถในการผ่อนชำระ").WebEdit("LoanAmount").Set

Call FW_WebEditAndVerifyResult ("กรอกอัตราดอกเบี้ย","คำนวณความสามารถในการกู้","คำนวณความสามารถในการผ่อนชำระ","InterestRate",InterestRate,ExpectedInterestRate)
'Browser("คำนวณความสามารถในการกู้").Page("คำนวณความสามารถในการผ่อนชำระ").WebEdit("InterestRate").Set

Call FW_WebEditAndVerifyResult ("กรอกอัตราดอกเบี้ย","คำนวณความสามารถในการกู้","คำนวณความสามารถในการผ่อนชำระ","LoanTotalYear",LoanTotalYear,ExpectedLoanTotalYear)
'Browser("คำนวณความสามารถในการกู้").Page("คำนวณความสามารถในการผ่อนชำระ").WebEdit("LoanTotalYear").Set @@ script infofile_;_ZIP::ssf4.xml_;_

Call FW_WebButton ("กดปุ่มเริ่มคำนวณ","คำนวณความสามารถในการกู้","คำนวณความสามารถในการผ่อนชำระ","เริ่มคำนวณ")
'Browser("คำนวณความสามารถในการกู้").Page("คำนวณความสามารถในการผ่อนชำระ").WebButton("เริ่มคำนวณ").Click @@ script infofile_;_ZIP::ssf5.xml_;_

Call FW_ValidateElementMessage ("ตรวจสอบคำค่างวดต่อเดือน","คำนวณความสามารถในการกู้","คำนวณความสามารถในการผ่อนชำระ","p_lt_ctl19_ksP_p_lt_ctl03_CalP","ผลการคำนวณ") @@ script infofile_;_ZIP::ssf6.xml_;_

'Browser("คำนวณความสามารถในการกู้").Page("คำนวณความสามารถในการผ่อนชำระ").WebElement("p_lt_ctl19_ksP_p_lt_ctl03_CalP").Click

Call FW_ValidateElementMessage ("ตรวจสอบคำนวณความสามารถในการกู้","คำนวณความสามารถในการกู้","คำนวณความสามารถในการผ่อนชำระ","p_lt_ctl19_ksP_p_lt_ctl03_CalP_2",ExpectedPeriodAmount)

'Browser("คำนวณความสามารถในการกู้").Page("คำนวณความสามารถในการผ่อนชำระ").WebElement("p_lt_ctl19_ksP_p_lt_ctl03_CalP_2").Click @@ script infofile_;_ZIP::ssf7.xml_;_

Call FW_CloseWebBrowserIE("ปิดเว็บ")

 @@ script infofile_;_ZIP::ssf10.xml_;_


 @@ script infofile_;_ZIP::ssf18.xml_;_
